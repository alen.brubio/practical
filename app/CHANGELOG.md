## [0.2.0] - 2019-09-25
### Added
- logout features


## [0.1.0] - 2019-09-20
### Added
- Logging in using the created account.
- Viewing of the stored trainees.
- Storing of new trainees.
- Updating and deleting stored trainees.

### Edited
- Layout of login view