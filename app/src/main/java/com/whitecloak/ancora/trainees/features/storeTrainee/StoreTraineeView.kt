package com.whitecloak.ancora.trainees.features.storeTrainee

interface StoreTraineeView {

    fun showLoading()
    fun hideLoading()
    fun showSuccess(message: String)
    fun showError(message: String)
    fun navigateToPrevious()
    fun setEmailError(message: String)
    fun setNameError(message: String)
}