package com.whitecloak.ancora.trainees.common.gateways

import android.content.Context
import com.whitecloak.ancora.trainees.common.models.User
import com.whitecloak.ancora.trainees.common.models.Token
import com.whitecloak.ancora.trainees.common.api.ServiceBuilder
import com.whitecloak.ancora.trainees.common.api.TraineeServices
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserGateway(context: Context) {

    private val sharedPref = context.getSharedPreferences(
        "com.whitecloak.example.practical4.PREFERENCE_FILE_KEY",
        Context.MODE_PRIVATE
    )

    interface CallBack {
        fun onFailed(message: String)
        fun onSuccess(message: String)
    }

    private val serviceBuilder = ServiceBuilder.buildService(TraineeServices::class.java)

    fun login(email: String, password: String, callback: CallBack) {

        val loginModel = User(email, password)

        serviceBuilder.login(loginModel).enqueue(object : Callback<Token> {
            override fun onFailure(call: Call<Token>, t: Throwable) {
                callback.onFailed("Failed to retrieved data")
            }

            override fun onResponse(call: Call<Token>, response: Response<Token>) {
                if (response.isSuccessful) {

                    with(sharedPref.edit()) {
                        putString("token", "Bearer ${response.body()?.access_token}")
                        apply()
                    }

                    callback.onSuccess("Success")
                } else {
                    callback.onFailed("Failed to retrieved data")
                }
            }
        })
    }

    fun checkTokenSession(callback: CallBack) {
        if (sharedPref.getString("token", null) != null) {
            callback.onSuccess("Success")
        }
    }

    fun logout() : Boolean{
        with(sharedPref.edit()) {
            clear()
            apply()
        }
        return true
    }
}
