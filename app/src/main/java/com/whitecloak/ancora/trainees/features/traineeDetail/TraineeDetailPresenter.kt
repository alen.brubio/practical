package com.whitecloak.ancora.trainees.features.traineeDetail

import com.whitecloak.ancora.trainees.common.gateways.TraineeGateway

class TraineeDetailPresenter(
    private val traineeDetailView: TraineeDetailView,
    private val traineeGateway: TraineeGateway
) : TraineeGateway.CallBack<String> {

    override fun onFailed(message: String) {
        traineeDetailView.showError(message)
        traineeDetailView.hideLoading()
    }

    override fun onSuccess(data: String) {
        traineeDetailView.showSuccess(data)
        traineeDetailView.hideLoading()
        traineeDetailView.navigateToPrevious()
    }

    fun updateTrainee(id: Int, name: String, email: String, group: String) {
        if (validateCredentials(name, email)) {
            traineeGateway.updateTrainee(id, name, email, group, this)
        }
    }

    fun deleteTrainee(id: Int) {
        traineeGateway.deleteTrainee(id, this)
    }

    fun showDialog(title: String, message: String, id: Int) {
        traineeDetailView.showDialog(title, message, id)
    }

    fun navigationUp() {
        traineeDetailView.navigateToPrevious()
    }

    private val emailRegex = Regex("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+")

    private fun validateCredentials(name: String, email: String): Boolean {
        if (email.isEmpty() && name.isEmpty()) {
            traineeDetailView.setEmailError("email must not be empty")
            traineeDetailView.setNameError("name must not be empty")
            return false
        }

        if (!email.matches(emailRegex)) {
            traineeDetailView.setEmailError("invalid email")
            return false
        }

        if (email.isEmpty()) {
            traineeDetailView.setEmailError("email must not be empty")
            return false
        }

        if (name.isEmpty()) {
            traineeDetailView.setNameError("name must not be empty")
            return false
        }

        return true
    }
}