package com.whitecloak.ancora.trainees.common.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.whitecloak.ancora.trainees.common.models.TraineeProfile

@Database(
    entities = [TraineeProfile::class],
    version = 1
)

abstract class TraineeDatabase : RoomDatabase() {
    abstract fun traineesDao(): TraineeDao
}