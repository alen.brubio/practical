package com.whitecloak.ancora.trainees.common.api

import com.whitecloak.ancora.trainees.common.models.AllTrainees
import com.whitecloak.ancora.trainees.common.models.TraineeRequest
import com.whitecloak.ancora.trainees.common.models.User
import com.whitecloak.ancora.trainees.common.models.Token
import com.whitecloak.ancora.trainees.common.models.TraineeResponse
import retrofit2.Call
import retrofit2.http.*


interface TraineeServices {

    @POST("/api/auth/login")
    fun login(@Body login: User): Call<Token>

    @GET("/api/trainees")
    fun getTraineesByGroup(
        @Header("Authorization") accessToken: String,
        @Query("group") group: String
    ): Call<AllTrainees>

    @PUT("/api/trainees/{id}")
    fun updateTrainee(
        @Header("Authorization") accessToken: String,
        @Path("id") id: Int,
        @Body traineeRequest: TraineeRequest
    ): Call<TraineeResponse>

    @POST("/api/trainees")
    fun storeTrainee(
        @Header("Authorization") accessToken: String,
        @Body traineeRequest: TraineeRequest
    ): Call<TraineeResponse>

    @DELETE("/api/trainees/{id}")
    fun deleteTrainee(
        @Header("Authorization") accessToken: String,
        @Path("id") id: Int
    ): Call<Void>

}