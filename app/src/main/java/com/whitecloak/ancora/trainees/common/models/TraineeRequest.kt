package com.whitecloak.ancora.trainees.common.models

import kotlinx.serialization.Serializable

@Serializable
data class TraineeRequest(
    val name: String,
    val email: String,
    val group: String
)
