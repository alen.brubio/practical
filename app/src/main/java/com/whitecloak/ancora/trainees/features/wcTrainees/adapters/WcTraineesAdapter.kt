package com.whitecloak.ancora.trainees.features.wcTrainees.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.whitecloak.ancora.trainees.R
import com.whitecloak.ancora.trainees.common.models.TraineeProfile
import kotlinx.android.synthetic.main.trainee_list.view.*

class WcTraineesAdapter (val data: List<TraineeProfile>) :
    RecyclerView.Adapter<CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.trainee_list, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {

        holder.v.tvName.text = data[position].name
        holder.v.tvEmail.text = data[position].email

        val bundle = Bundle()
        bundle.putString("name", data[position].name)
        bundle.putString("email", data[position].email)
        bundle.putInt("id", data[position].id)
        bundle.putString("group", data[position].group)

        holder.v.setOnClickListener {
            it.findNavController().navigate(R.id.action_wctrainees_to_trainee_details, bundle)

        }
    }
}

class CustomViewHolder(var v: View) : RecyclerView.ViewHolder(v)
