package com.whitecloak.ancora.trainees.features.traineeDetail

interface TraineeDetailView {

    fun setEmailError(message: String)
    fun showDialog(title: String, message: String, id: Int)
    fun showError(message: String)
    fun showSuccess(message: String)
    fun showLoading()
    fun hideLoading()
    fun navigateToPrevious()
    fun setNameError(message: String)
}