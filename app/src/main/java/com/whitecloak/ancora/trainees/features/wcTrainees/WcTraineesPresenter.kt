package com.whitecloak.ancora.trainees.features.wcTrainees

import com.whitecloak.ancora.trainees.common.gateways.TraineeGateway
import com.whitecloak.ancora.trainees.common.gateways.UserGateway
import com.whitecloak.ancora.trainees.common.models.TraineeProfile

class WcTraineesPresenter(
    private val wcTraineesView: WcTraineesView,
    private val userGateway: UserGateway,
    private val traineeGateway: TraineeGateway
) : TraineeGateway.CallBack<List<TraineeProfile>> {


    override fun onSuccess(data: List<TraineeProfile>) {
        wcTraineesView.showTrainees(data)
        wcTraineesView.hideLoading()
    }

    override fun onFailed(message: String) {
        wcTraineesView.hideLoading()
    }

    fun getTraineeByGroup(group: String) {
        wcTraineesView.showLoading()
        traineeGateway.getTraineesByGroup(group, this)
    }

    fun navigateToStoreTrainee() {
        wcTraineesView.navigateToStoreTrainee()
    }

    fun showDialog(){
        wcTraineesView.showDialog("Logout", "Are you sure you want to logout?")
    }

    fun logout(){
        if(userGateway.logout()){
            wcTraineesView.navigateToPrevious()
        }

    }


}