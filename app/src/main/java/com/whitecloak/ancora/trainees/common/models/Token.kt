package com.whitecloak.ancora.trainees.common.models

import kotlinx.serialization.Serializable

@Serializable
data class Token(
    val access_token: String,
    val token_type: String,
    val expires_at: String
)