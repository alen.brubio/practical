package com.whitecloak.ancora.trainees.common.models

import kotlinx.serialization.Serializable

@Serializable
data class User(
    val email: String,
    val password: String
)
