package com.whitecloak.ancora.trainees.features.login

interface LoginView {

    fun showLoading()
    fun hideLoading()
    fun setPasswordError(message: String)
    fun setEmailError(message: String)
    fun loginSuccess(message: String)
    fun loginFailure(message: String)
    fun navigateToWcTrainees()
}