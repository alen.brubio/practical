package com.whitecloak.ancora.trainees.features.traineeDetail


import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.whitecloak.ancora.trainees.R
import com.whitecloak.ancora.trainees.common.gateways.TraineeGateway
import kotlinx.android.synthetic.main.fragment_trainee_details.*


class TraineeDetailFragment : Fragment(), TraineeDetailView {
    override fun setNameError(message: String) {
        etName_detail.error = message
    }

    override fun setEmailError(message: String) {
        etEmail_details.error = message
    }

    private lateinit var traineeDetailPresenter: TraineeDetailPresenter

    override fun navigateToPrevious() {
        findNavController().navigateUp()
    }

    override fun showDialog(title: String, message: String, id: Int) {
        AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setNegativeButton("cancel") { DialogInterface, i -> DialogInterface.dismiss() }
            .setPositiveButton("ok") { DialogInterface, i ->
                traineeDetailPresenter.deleteTrainee(
                    id
                )
            }
            .create()
            .show()
    }

    override fun showError(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun showSuccess(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun showLoading() {
        pb_trainee_detail.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        pb_trainee_detail.visibility = View.GONE
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_trainee_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        traineeDetailPresenter = TraineeDetailPresenter(this, TraineeGateway(context!!))

        val id = arguments?.getInt("id")
        val name = arguments?.getString("name").toString()
        val email = arguments?.getString("email").toString()

        when (arguments?.getString("group").toString()) {
            "frontend" -> {
                spinner_trainee_details.setSelection(0)
            }
            "backend" -> {
                spinner_trainee_details.setSelection(1)
            }
            "mobile" -> {
                spinner_trainee_details.setSelection(2)
            }
        }

        etName_detail.setText(name)
        etEmail_details.setText(email)


        btnSave.setOnClickListener {
            traineeDetailPresenter.updateTrainee(
                id!!.toInt(),
                etName_detail.text.toString(),
                etEmail_details.text.toString(),
                spinner_trainee_details.selectedItem.toString()
            )
        }

        toolbar_trainee_details.setOnMenuItemClickListener {

            when (it.itemId) {
                R.id.action_delete -> {
                    traineeDetailPresenter.showDialog(
                        "Delete",
                        "Are you sure you want to delete?",
                        id!!
                    )
                    return@setOnMenuItemClickListener true
                }
                else -> {
                    return@setOnMenuItemClickListener true
                }
            }
        }

        toolbar_trainee_details.setNavigationOnClickListener {
            traineeDetailPresenter.navigationUp()
        }

    }
}
