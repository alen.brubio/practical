package com.whitecloak.ancora.trainees.features.login


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.whitecloak.ancora.trainees.R
import com.whitecloak.ancora.trainees.common.gateways.UserGateway
import kotlinx.android.synthetic.main.fragment_login_fragment.*


class LoginFragment : Fragment(), LoginView {
    override fun setPasswordError(message: String) {
        password.error = message
    }

    override fun navigateToWcTrainees() {
        findNavController().navigate(R.id.action_login_fragment_to_wctrainees)
    }

    override fun loginFailure(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override fun loginSuccess(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override fun showLoading() {
        pb_login_trainee.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        pb_login_trainee.visibility = View.GONE
    }

    override fun setEmailError(message: String) {
        email.error = message
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val loginPresenter = LoginPresenter(this, UserGateway(context!!))

        val sharedPref = this.context!!.getSharedPreferences(
            "com.whitecloak.example.practical4.PREFERENCE_FILE_KEY",
            Context.MODE_PRIVATE
        )

        loginPresenter.checkTokenSession()

        btnLogin.setOnClickListener {
            loginPresenter.login(
                email.text.toString(),
                password.text.toString()
            )
        }
    }
}
