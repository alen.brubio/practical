package com.whitecloak.ancora.trainees.common.models

import kotlinx.serialization.Serializable

@Serializable
data class TraineeResponse(val data: TraineeProfile)
