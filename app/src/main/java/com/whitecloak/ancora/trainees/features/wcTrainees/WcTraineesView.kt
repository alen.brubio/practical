package com.whitecloak.ancora.trainees.features.wcTrainees

import com.whitecloak.ancora.trainees.common.models.TraineeProfile

interface WcTraineesView {

    fun showTrainees(data: List<TraineeProfile>)
    fun showLoading()
    fun hideLoading()
    fun navigateToStoreTrainee()
    fun showDialog(title: String, message: String)
    fun navigateToPrevious()
}