package com.whitecloak.ancora.trainees.common.gateways


import android.content.Context
import androidx.room.Room
import com.whitecloak.ancora.trainees.common.models.TraineeRequest
import com.whitecloak.ancora.trainees.common.database.TraineeDatabase
import com.whitecloak.ancora.trainees.common.api.ServiceBuilder
import com.whitecloak.ancora.trainees.common.api.TraineeServices
import com.whitecloak.ancora.trainees.common.models.TraineeResponse
import com.whitecloak.ancora.trainees.common.models.AllTrainees
import com.whitecloak.ancora.trainees.common.models.TraineeProfile
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class TraineeGateway(context: Context) {


    interface CallBack<T> {
        fun onFailed(message: String)
        fun onSuccess(data: T)
    }

    private val sharedPref = context.getSharedPreferences(
        "com.whitecloak.example.practical4.PREFERENCE_FILE_KEY",
        Context.MODE_PRIVATE
    )

    private val token = sharedPref.getString("token", null)

    private val db = Room.databaseBuilder(
        context,
        TraineeDatabase::class.java, "TraineeDatabase.db"
    )
        .allowMainThreadQueries()
        .build()

    private val traineeServices = ServiceBuilder.buildService(TraineeServices::class.java)

    fun getTraineesByGroup(group: String, callback: CallBack<List<TraineeProfile>>) {
        traineeServices.getTraineesByGroup(token!!, group)
            .enqueue(object : Callback<AllTrainees> {

                override fun onFailure(call: Call<AllTrainees>, t: Throwable) {

                    val data = db.traineesDao().getTraineeByGroup(group)
                    callback.onSuccess(data)
                }

                override fun onResponse(
                    call: Call<AllTrainees>,
                    response: Response<AllTrainees>
                ) {
                    val data = response.body()?.data

                    callback.onSuccess(data!!)
                }
            })

    }

    fun storeTrainee(
        name: String,
        email: String,
        group: String,
        callback: CallBack<String>
    ) {

        val storeTrainee = TraineeRequest(name, email, group)
        traineeServices.storeTrainee(token!!, storeTrainee)
            .enqueue(object : Callback<TraineeResponse> {
                override fun onFailure(call: Call<TraineeResponse>, t: Throwable) {
                    callback.onFailed("Failed to add")
                }

                override fun onResponse(
                    call: Call<TraineeResponse>,
                    response: Response<TraineeResponse>
                ) {
                    val data = response.body()!!.data
                    val traineeProfile = TraineeProfile(
                        data.id,
                        data.name,
                        data.email,
                        data.group
                    )
                    db.traineesDao().insert(traineeProfile)
                    callback.onSuccess("Added Successfully")

                }
            })
    }

    fun deleteTrainee(id: Int, callback: CallBack<String>) {
        traineeServices.deleteTrainee(token!!, id).enqueue(object : Callback<Void> {
            override fun onFailure(call: Call<Void>, t: Throwable) {
                callback.onFailed("Failed to delete")
            }

            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                callback.onSuccess("Deleted Successfully")
                db.traineesDao().delete(id)
            }
        })

    }

    fun updateTrainee(
        id: Int,
        name: String,
        email: String,
        group: String,
        callback: CallBack<String>
    ) {
        val updateTraineeRequest = TraineeRequest(name, email, group)

        val traineeProfile = TraineeProfile(id, name, email, group)

        traineeServices.updateTrainee(token!!, id, updateTraineeRequest)
            .enqueue(object : Callback<TraineeResponse> {
                override fun onFailure(call: Call<TraineeResponse>, t: Throwable) {
                    callback.onFailed("Failed to update")
                }

                override fun onResponse(
                    call: Call<TraineeResponse>,
                    response: Response<TraineeResponse>
                ) {
                    callback.onSuccess("Updated Successfully")
                    db.traineesDao().updateTrainee(traineeProfile)
                }
            })

    }
}