package com.whitecloak.ancora.trainees.features.storeTrainee

import com.whitecloak.ancora.trainees.common.gateways.TraineeGateway

class StoreTraineePresenter(
    private val storeTraineeView: StoreTraineeView,
    private val traineeGateway: TraineeGateway
) : TraineeGateway.CallBack<String> {

    override fun onFailed(message: String) {
        storeTraineeView.showError(message)
        storeTraineeView.hideLoading()
    }

    override fun onSuccess(data: String) {
        storeTraineeView.showSuccess(data)
        storeTraineeView.hideLoading()
        storeTraineeView.navigateToPrevious()
    }

    fun storeTrainee(name: String, email: String, group: String) {
        if (validateCredentials(name, email)) {
            traineeGateway.storeTrainee(name, email, group, this)
        }
    }

    fun navigationUp() {
        storeTraineeView.navigateToPrevious()
    }

    private val emailRegex = Regex("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+")

    private fun validateCredentials(name: String, email: String): Boolean {
        if (email.isEmpty() && name.isEmpty()) {
            storeTraineeView.setEmailError("email must not be empty")
            storeTraineeView.setNameError("name must not be empty")
            return false
        }

        if (!email.matches(emailRegex)) {
            storeTraineeView.setEmailError("invalid email")
            return false
        }

        if (email.isEmpty()) {
            storeTraineeView.setEmailError("email must not be empty")
            return false
        }

        if (name.isEmpty()) {
            storeTraineeView.setNameError("name must not be empty")
            return false
        }
        return true
    }
}