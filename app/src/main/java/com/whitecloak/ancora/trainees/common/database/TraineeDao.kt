package com.whitecloak.ancora.trainees.common.database

import androidx.room.*
import com.whitecloak.ancora.trainees.common.models.TraineeProfile

@Dao
interface TraineeDao {

    @Query("SELECT * FROM TraineeProfile")
    fun getAll(): List<TraineeProfile>

    @Query("SELECT * FROM TraineeProfile WHERE `group` LIKE :group")
    fun getTraineeByGroup(group: String): List<TraineeProfile>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(trainees: TraineeProfile)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(data: List<TraineeProfile>?)

    @Query("DELETE FROM TraineeProfile WHERE id = :id")
    fun delete(id: Int)

    @Update
    fun updateTrainee(trainees: TraineeProfile)

}
