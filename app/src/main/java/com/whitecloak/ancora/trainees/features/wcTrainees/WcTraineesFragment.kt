package com.whitecloak.ancora.trainees.features.wcTrainees

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.whitecloak.ancora.trainees.R
import com.whitecloak.ancora.trainees.common.models.TraineeProfile
import com.whitecloak.ancora.trainees.common.gateways.TraineeGateway
import com.whitecloak.ancora.trainees.common.gateways.UserGateway
import com.whitecloak.ancora.trainees.features.wcTrainees.adapters.WcTraineesAdapter
import kotlinx.android.synthetic.main.fragment_wctrainees.*


class WcTraineesFragment : Fragment(), WcTraineesView {

    private lateinit var wcTraineesPresenter: WcTraineesPresenter

    override fun showDialog(title: String, message: String) {
        AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setNegativeButton("cancel") { DialogInterface, i -> DialogInterface.dismiss() }
            .setPositiveButton("ok") { DialogInterface, i ->
                wcTraineesPresenter.logout()
            }
            .create()
            .show()
    }

    override fun navigateToPrevious() {
        findNavController().navigate(R.id.action_wctrainees_to_login_fragment)
    }

    override fun navigateToStoreTrainee() {
        findNavController().navigate(R.id.action_wctrainees_to_add_trainee)
    }

    override fun showTrainees(data: List<TraineeProfile>) {
        recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        recyclerView.adapter = WcTraineesAdapter(data)
    }

    override fun showLoading() {
        pb_wctrainees.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        pb_wctrainees.visibility = View.GONE
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wctrainees, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        wcTraineesPresenter = WcTraineesPresenter(
            this, UserGateway(context!!),
            TraineeGateway(context!!)
        )

        wcTraineesPresenter.getTraineeByGroup("mobile")

        bottomNavigationView.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.front_end -> {
                    wcTraineesPresenter.getTraineeByGroup("frontend")
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.back_end -> {
                    wcTraineesPresenter.getTraineeByGroup("backend")
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.mobile_dev -> {
                    wcTraineesPresenter.getTraineeByGroup("mobile")
                    return@setOnNavigationItemSelectedListener true
                }
                else -> {
                    return@setOnNavigationItemSelectedListener true
                }
            }
        }

        toolbar_wctrainees.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.action_add -> {
                    wcTraineesPresenter.navigateToStoreTrainee()
                    return@setOnMenuItemClickListener true
                }
                else -> {
                    return@setOnMenuItemClickListener true
                }
            }
        }
        toolbar_wctrainees.setNavigationOnClickListener {
            wcTraineesPresenter.showDialog()
        }

    }
}

