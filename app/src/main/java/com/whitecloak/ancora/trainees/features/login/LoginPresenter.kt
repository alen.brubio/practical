package com.whitecloak.ancora.trainees.features.login

import com.whitecloak.ancora.trainees.common.gateways.UserGateway


class LoginPresenter(private val loginView: LoginView, private val userGateway: UserGateway) :
    UserGateway.CallBack {

    override fun onFailed(message: String) {
        loginView.loginFailure(message)
        loginView.hideLoading()
    }

    override fun onSuccess(message: String) {
        loginView.loginSuccess(message)
        loginView.hideLoading()
        loginView.navigateToWcTrainees()
    }

    private val emailRegex = Regex("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+")

    fun login(email: String, password: String) {


        if (validateCredentials(email, password)) {
            loginView.showLoading()
            userGateway.login(email, password, this)
        }
    }

    private fun validateCredentials(email: String, password: String): Boolean {
        if (email.isEmpty() && password.isEmpty()) {
            loginView.setEmailError("email must not be empty")
            loginView.setPasswordError("password must not be empty")
            return false
        }

        if (!email.matches(emailRegex)) {
            loginView.setEmailError("invalid email")
            return false
        }

        if (email.isEmpty()) {
            loginView.setEmailError("email must not be empty")
            return false
        }

        if (password.isEmpty()) {
            loginView.setPasswordError("password must not be empty")
            return false
        }
        return true
    }

    fun checkTokenSession() {
        userGateway.checkTokenSession(this)
    }
}
