package com.whitecloak.ancora.trainees.features.storeTrainee


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.whitecloak.ancora.trainees.R
import com.whitecloak.ancora.trainees.common.gateways.TraineeGateway
import kotlinx.android.synthetic.main.fragment_store_trainee.*


class StoreTraineeFragment : Fragment(), StoreTraineeView {
    override fun setNameError(message: String) {
        etName.error = message
    }

    override fun setEmailError(message: String) {
        etEmail.error = message
    }

    override fun navigateToPrevious() {
        findNavController().navigateUp()
    }

    override fun hideLoading() {
        pb_add_trainee.visibility = View.GONE
    }

    override fun showSuccess(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun showError(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun showLoading() {
        pb_add_trainee.visibility = View.VISIBLE
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_store_trainee, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val storeTraineePresenter = StoreTraineePresenter(this, TraineeGateway(context!!))

        btnCreate.setOnClickListener {
            storeTraineePresenter.storeTrainee(
                etName.text.toString(),
                etEmail.text.toString(),
                spinner_store_trainee.selectedItem.toString()
            )
        }

        toolbar_store_details.setNavigationOnClickListener {
            storeTraineePresenter.navigationUp()
        }
    }
}
